
class DataTable
{
    constructor(config){
        this._self = $("<table></table>");
        this._head = $("<thead></thead>");
        this._body = $("<tbody></tbody>");
        this._row = $("<tr></tr>");
        this._column = $("<td></td>");
        this._setting = {
            container: {
                selector: {
                    id: "",
                    class: "wrap-table"
                }
            },
            selector: {
                id: "",
                class: ""
            }, 
            dataset: [

            ]
        }

        this.setting = config;
        this.update();
    }

    set setting(config){
        if(_$_.isValidJSON(config)){
            this._setting = $.extend(true, {}, this._setting, config);
        }
    }
    get setting(){ return this._setting; }

    get self(){ return this._self; }
    get head(){ return this._head; }
    get body(){ return this._body; }

    update(){
        setTimeout(() => {
            this.build();
        }, 0);
    }

    async build(){
        if(typeof this.setting.id === "string"){
            if(this.setting.id !== undefined){
                this.self.prop("id", this.setting.id);
            }

            if(this.setting.class !== undefined){
                this.self.addClass(this.setting.class);
            }
        }

        
    
    }
}