//Show payment page in full panel.
    const $goto_payment = $("#goto-panel-payment");
    const $goto_group_tables = $(".goto-panel-group-tables");
    const $goto_group_items = $("#goto-panel-group-items");

    $goto_payment.click(function(e){
        $("#panel-payment").addClass("show");
        $("#panel-group-tables").removeClass("show");
        $("#panel-group-items").addClass("hide");
        $(".nav-header .min-max").removeClass("show");
        setTimeout(() => {
            $("#panel-payment input#cash").focus();
        }, 500);  
    });

    $goto_group_tables.click(function(e){
        $("#panel-group-tables").addClass("show");
        $("#panel-payment").removeClass("show");
        $("#panel-group-items").removeClass("show");
        $(".nav-header .min-max").removeClass("show");
    });

    $goto_group_items.click(function(e){
        $("#panel-payment").removeClass("show");
        $("#panel-group-tables").removeClass("show");
        $(".nav-header .min-max").addClass("show");
    });