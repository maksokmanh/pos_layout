
const number_pad = $("#panel-payment .number-pad .grid");
const quick_number = $("#panel-payment .quick-number .grid");
let value = "";
var input_flag = $();
$(".number").val(0);
$(".number").prop("maxlength", 12);
number_pad.click(function(e){
    let input_value = input_flag.value;
    if(input_value !== undefined && input_value.length <= 12){
        setTimeout(() => {
            input_flag.selectionStart = input_flag.value.length;
            $(input_flag).focus();
        }, 0);
        
        value = input_value + $(this).data("value").toString();
        value = _$_.validNumber(value);
        input_flag.value = value;
        if($(this).data("value") === -1){
            input_flag.value = input_value.substring(0, input_value.length - 1);
            if(input_flag.value === ""){
                input_flag.value = 0;
            }
        }
    }   

});

let q_val = 0;
quick_number.click(function(e){
    q_val = parseFloat($(input_flag).val());
    q_val += parseFloat($(this).data("value"));
    $(input_flag).val(q_val);
});

$("input[class=number]").focus(function(e){
    input_flag = this;
});

let timer;
$(".number").on("keyup", function(e){
    clearTimeout(timer);
    timer = setTimeout(() => {
        if(parseFloat(this.value) === 0){
            this.value = 0;
        }
    }, 1500);

    if(this.value === "" && e.keyCode === 8){
        this.value = 0;
    }

    this.value = _$_.validNumber(this.value);
    
});

