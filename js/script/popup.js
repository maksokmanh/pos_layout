
// let ajax_loader = new AjaxLoader("images/loader/loader.svg");
// ajax_loader.hide();
let dlg = new DialogBox({
    // close_button: false,
    position: "top-center",
    content: {
        selector: "#admin-authorization",
        class: "login"
    }, 
    icon: "fas fa-lock",
    button: {
        ok: {
            text: "Login",
            callback: function(e){
                this.meta.setting.icon = "fas fa-lock fa-spin";
                this.meta.build(this.setting);
                setTimeout(() => {
                    this.meta.build(this.setting);
                    this.meta.setting.icon = "fas fa-unlock-alt";
                    setTimeout(() => {
                        this.meta.shutdown();
                    }, 500);
                }, 2000);
                
            }
        }
    }
});

// dlg.shutdown("during", function(e){
//     console.log("after shutdown");
// });

$("#goto-main-menu").click(function(e){
    
    let msg = new DialogBox(
        {
            icon: "far fa-list-alt",
            close_button: true,
            content: {
                selector: "#main-menu-content",
            },
            caption: "Menu",
            position: "top-center",
            type: "ok"
        }
    );
    msg.setting.button.ok.text = "Cancel";
    msg.setting.animation.shutdown.animation_type = "slide-up";
    msg.confirm(function(e){
        this.meta.shutdown();
    });
});

 $("#goto-setting").click(function(e){
    let dlg = new DialogBox({
        position: "top-center",
        caption: "Setting",
        content: {
            selector: "#setting-content"
        },
        button: {
            ok: {
                text: "Done"
            }
        },
        icon: "fas fa-cogs"
    });
});

$("#dbl-move-table").click(function(e){
    let msg = new DialogBox(
        {
            icon: {
                class: "fas fa-table"
            },
            content: { selector: "#move-table"},
            class: "outer-content",
            caption: "Move Table",
            position: "top-center",
            type: "ok-cancel",
            close_button: true
        }
    );

    msg.setting.button.ok.text = "Move";
    msg.setting.animation.shutdown.animation_type = "slide-up";
    msg.setting.type = "yes-no-cancel";

    $("#move-table-breadcrumb .step").click(function(e){
        console.log(this)
    });
    
    $("#move-table-list .wrap-grid .grid input[name='table']").off();
    $("#move-table-list .wrap-grid .grid input[name='table']").click(function(e){
        console.log(this)
    });
});

$("#dbl-combine-receipt").click(function(e){
    let msg = new DialogBox(
        {
            class: "outer-content",
            caption: "Combine Receipt",
            content: {selector: "#combine-receipt"},
            position: "top-center",
            type: "ok-cancel",
            close_button: true
        }
    );
    msg.setting.animation.shutdown.animation_type = "slide-up";
    msg.setting.button.ok.text = "Combine";

    $("#combine-receipt-list .wrap-grid .grid input[name='receipt']").click(function(e){
        console.log(this)
    });
    
});

$("#dbl-split-receipt").click(function(e){
    $.bindRows("#split-receipt-listview", $.getItems(), "id");
    let msg = new DialogBox(
        {
            class: "outer-content",
            caption: "Split Receipt",
            content: {selector: "#split-receipt"},
            position: "top-center",
            type: "ok-cancel",
            close_button: true
        }
    );

    msg.setting.animation.shutdown.animation_type = "slide-up";
    msg.setting.button.ok.text = "Split";
});

$("#btn-discount").click(function(e){
    $.bindRows("#membercard-discount-listview", item_tables, "id");
    let msg = new DialogBox(
        {
            class: "outer-content",
            caption: "Discount Member Card",
            content: {selector: "#membercard-discount"},
            position: "top-center",
            type: "ok-cancel",
            button: {
                ok: {
                    text: "Apply"
                }
            },
            animation: {
                shutdown: {
                    animation_type: "slide-up"
                }
            }
        }
    );
    
});