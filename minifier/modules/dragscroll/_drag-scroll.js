$.fn.dragScroll = function(options = {}){
  var cur_down = false;
  var cur_x_pos = 0;
  var cur_y_pos = 0;
  var container = $(this);
  container.addClass("ds-grab");

  //Check if container is used or not
  if(typeof container === "undefined") {
    return;
  }

  $(window).mousemove(function(m){
    if(cur_down === true){
      var cal_pos_x = (cur_x_pos - m.pageX);
      var cal_pos_y = (cur_y_pos - m.pageY);
      container.scrollLeft(cal_pos_x);
      container.scrollTop(cal_pos_y);
    }
  });

  container.mousedown(function(m){
    container.removeClass("ds-grab");
    container.addClass("ds-grabbing");
    cur_down = true;
    cur_x_pos = m.pageX + $(this).scrollLeft();
    cur_y_pos = m.pageY + $(this).scrollTop();
    
    m.preventDefault();
  });

  $(window).mouseup(function(){
    cur_down = false;
    container.removeClass("ds-grabbing");
    container.addClass("ds-grab");
  });
}